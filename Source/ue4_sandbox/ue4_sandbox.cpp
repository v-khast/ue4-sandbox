// Copyright Epic Games, Inc. All Rights Reserved.

#include "ue4_sandbox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ue4_sandbox, "ue4_sandbox" );
 