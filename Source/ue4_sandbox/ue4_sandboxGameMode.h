// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ue4_sandboxGameMode.generated.h"

UCLASS(minimalapi)
class Aue4_sandboxGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aue4_sandboxGameMode();
};



