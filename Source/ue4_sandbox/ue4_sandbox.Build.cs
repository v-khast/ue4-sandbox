// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ue4_sandbox : ModuleRules
{
	public ue4_sandbox(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
