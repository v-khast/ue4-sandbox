// Copyright Epic Games, Inc. All Rights Reserved.

#include "ue4_sandboxGameMode.h"
#include "ue4_sandboxCharacter.h"
#include "UObject/ConstructorHelpers.h"

Aue4_sandboxGameMode::Aue4_sandboxGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
